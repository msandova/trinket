use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::{gdk, gio, glib};
use trinket::prelude::*;

use gettextrs::gettext;

use crate::application::Application;
use crate::config::{APP_ID, PROFILE};

mod imp {
    use super::*;

    use adw::subclass::prelude::*;
    use gtk::CompositeTemplate;

    #[derive(Debug, CompositeTemplate)]
    #[template(resource = "/org/gnome/TrinketDemo/ui/window.ui")]
    pub struct Window {
        #[template_child]
        pub progress_icon: TemplateChild<trinket::ProgressIcon>,
        #[template_child]
        pub running_switch: TemplateChild<gtk::Switch>,
        #[template_child]
        pub qrentry: TemplateChild<gtk::Entry>,
        #[template_child]
        pub qrcode: TemplateChild<trinket::QRCode>,
        #[template_child]
        pub qrcode_button: TemplateChild<gtk::Button>,
        #[template_child]
        pub welcome: TemplateChild<adw::StatusPage>,
        #[template_child]
        pub svg_picture: TemplateChild<gtk::Picture>,
        #[template_child]
        pub drop_overlay: TemplateChild<trinket::DropOverlay>,

        pub settings: gio::Settings,
    }

    impl Default for Window {
        fn default() -> Self {
            Self {
                progress_icon: TemplateChild::default(),
                running_switch: TemplateChild::default(),
                qrcode: TemplateChild::default(),
                qrentry: TemplateChild::default(),
                qrcode_button: TemplateChild::default(),
                welcome: TemplateChild::default(),
                svg_picture: TemplateChild::default(),
                drop_overlay: TemplateChild::default(),

                settings: gio::Settings::new(APP_ID),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Window {
        const NAME: &'static str = "Window";
        type Type = super::Window;
        type ParentType = adw::ApplicationWindow;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);

            klass.install_action("select-svg", None, move |obj, _, _| {
                obj.select_svg();
            });
        }

        // You must call `Widget`'s `init_template()` within `instance_init()`.
        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for Window {
        fn constructed(&self, obj: &Self::Type) {
            self.parent_constructed(obj);

            // Devel Profile
            if PROFILE == "Devel" {
                obj.add_css_class("devel");
                self.welcome.set_icon_name(Some(APP_ID));
            }

            // Load latest window state
            obj.load_window_size();

            let duration = std::time::Duration::from_millis(10);
            glib::timeout_add_local(
                duration,
                glib::clone!(@weak obj => @default-return glib::Continue(false), move || {
                    let self_ = imp::Window::from_instance(&obj);
                    if self_.running_switch.is_active() {
                        let progress = (self_.progress_icon.progress() + 0.001) % 1.0;
                        self_.progress_icon.set_progress(progress);
                    };

                    glib::Continue(true)
                }),
            );

            self.qrentry
                .connect_changed(glib::clone!(@weak obj => move |entry| {
                    let self_ = imp::Window::from_instance(&obj);

                    self_.qrcode.set_bytes(entry.text().as_bytes());
                }));

            self.qrcode_button
                .connect_clicked(glib::clone!(@weak obj => move |_| {
                    let dialog = gtk::FileChooserNativeBuilder::new()
                        .action(gtk::FileChooserAction::Save)
                        .transient_for(&obj)
                        .build();
                    dialog.set_current_name(&gettext("qr-code.png"));

                    dialog.connect_response(
                        glib::clone!(@strong obj, @strong dialog => move |_dialog, response| {
                            let self_ = imp::Window::from_instance(&obj);
                            if response == gtk::ResponseType::Accept {
                                let path = dialog.file().unwrap().path().unwrap();
                                self_.qrcode.save_to_png(path).unwrap_or_else(|_|
                                    log::warn!("Could not save file")
                                );
                            }
                        }),
                    );
                    dialog.show();
                }));

            let file = gio::File::for_uri("resource:///org/gnome/TrinketDemo/libtrinket.svg");

            let ctx = glib::MainContext::default();
            ctx.spawn_local(glib::clone!(@weak obj => async move {
                if let Ok((bytes, _)) = file.load_bytes_async_future().await {
                    let obj_ = imp::Window::from_instance(&obj);
                    let bytes = glib::Bytes::from_owned(bytes);
                    let paintable = trinket::SvgPaintable::new(&bytes);

                    obj.bind_property("scale-factor", &paintable, "scale-factor")
                          .flags(glib::BindingFlags::SYNC_CREATE)
                          .build();

                    obj_.svg_picture.set_paintable(Some(&paintable));
                };
            }));

            let self_ = imp::Window::from_instance(obj);

            let target = gtk::DropTarget::new(
                gio::File::static_type(),
                gdk::DragAction::COPY | gdk::DragAction::MOVE,
            );

            self_.drop_overlay.set_drop_target(&target);
        }
    }

    impl WidgetImpl for Window {}
    impl WindowImpl for Window {
        // Save window state on delete event
        fn close_request(&self, window: &Self::Type) -> gtk::Inhibit {
            if let Err(err) = window.save_window_size() {
                log::warn!("Failed to save window state, {}", &err);
            }

            // Pass close request on to the parent
            self.parent_close_request(window)
        }
    }
    impl AdwWindowImpl for Window {}
    impl ApplicationWindowImpl for Window {}
    impl AdwApplicationWindowImpl for Window {}
}

glib::wrapper! {
    pub struct Window(ObjectSubclass<imp::Window>)
        @extends gtk::Widget, gtk::Window, gtk::ApplicationWindow,
        @implements gio::ActionMap, gio::ActionGroup;
}

impl Window {
    pub fn new(app: &Application) -> Self {
        glib::Object::new(&[("application", app)]).expect("Failed to create Window")
    }

    fn save_window_size(&self) -> Result<(), glib::BoolError> {
        let self_ = imp::Window::from_instance(self);

        let (width, height) = self.default_size();

        self_.settings.set_int("window-width", width)?;
        self_.settings.set_int("window-height", height)?;

        self_
            .settings
            .set_boolean("is-maximized", self.is_maximized())?;

        Ok(())
    }

    fn load_window_size(&self) {
        let self_ = imp::Window::from_instance(self);

        let width = self_.settings.int("window-width");
        let height = self_.settings.int("window-height");
        let is_maximized = self_.settings.boolean("is-maximized");

        self.set_default_size(width, height);

        if is_maximized {
            self.maximize();
        }
    }

    pub fn select_svg(&self) {
        let dialog = gtk::FileChooserNative::new(
            Some(&gettext("Select Symbolic")),
            Some(self),
            gtk::FileChooserAction::Open,
            None,
            None,
        );

        let svg_filter = gtk::FileFilter::new();
        svg_filter.set_name(Some(&gettext("SVG images")));
        svg_filter.add_mime_type("image/svg+xml");

        dialog.add_filter(&svg_filter);

        dialog.connect_response(
            glib::clone!(@strong dialog, @weak self as widget => move |_, response| {
                if response == gtk::ResponseType::Accept {
                    let file = dialog.file().unwrap();

                    let ctx = glib::MainContext::default();
                    ctx.spawn_local(glib::clone!(@weak widget => async move {
                        if let Ok((bytes, _)) = file.load_bytes_async_future().await {
                            let widget_ = imp::Window::from_instance(&widget);
                            let bytes = glib::Bytes::from_owned(bytes);
                            let paintable = trinket::SvgPaintable::new(&bytes);

                            widget.bind_property("scale-factor", &paintable, "scale-factor")
                                .flags(glib::BindingFlags::SYNC_CREATE)
                                .build();

                            widget_.svg_picture.set_paintable(Some(&paintable));
                        };
                    }));
                }
                dialog.destroy();
            }),
        );
        dialog.show();
    }
}
