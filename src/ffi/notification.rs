use crate::Notification;

use glib::translate::*;
use gtk::glib;
use std::os::raw::c_char;

mod bind {
    use crate::notification::imp;
    use gtk::subclass::prelude::*;

    pub type Notification = <imp::Notification as ObjectSubclass>::Instance;
}

#[no_mangle]
pub extern "C" fn tri_notification_new(title: *const c_char) -> *mut bind::Notification {
    gtk::init().unwrap(); // FIXME We shouldn't call gtk::init().

    let title = unsafe { &*glib::GString::from_glib_borrow(title) };
    let obj = Notification::new(title);
    obj.to_glib_full()
}

#[no_mangle]
pub extern "C" fn tri_notification_get_type() -> glib::ffi::GType {
    gtk::init().unwrap(); // FIXME We shouldn't call gtk::init().

    <Notification as glib::StaticType>::static_type().into_glib()
}

#[no_mangle]
pub extern "C" fn tri_notification_set_button(
    this: *mut bind::Notification,
    label: *const c_char,
    detailed_action: *const c_char,
) {
    unsafe {
        let obj: glib::translate::Borrowed<Notification> = from_glib_borrow(this);
        let label = &*glib::GString::from_glib_borrow(label);
        let detailed_action = &*glib::GString::from_glib_borrow(detailed_action);
        obj.set_button(label, detailed_action);
    }
}

#[no_mangle]
pub extern "C" fn tri_notification_set_button_with_target_value(
    this: *mut bind::Notification,
    label: *const c_char,
    action: *const c_char,
    target: *mut glib::ffi::GVariant,
) {
    unsafe {
        let obj: glib::translate::Borrowed<Notification> = from_glib_borrow(this);
        let label = &*glib::GString::from_glib_borrow(label);
        let action = &*glib::GString::from_glib_borrow(action);
        let target: Option<glib::Variant> = from_glib_none(target);
        obj.set_button_with_target_value(&label, &action, target.as_ref());
    }
}
