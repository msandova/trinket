use crate::prelude::QRCodeExt;
use crate::QRCode;

use glib::translate::*;
use gtk::glib;

use std::os::raw::c_char;

mod bind {
    use crate::qr_code::imp;
    use gtk::subclass::prelude::*;

    pub type QRCode = <imp::QRCode as ObjectSubclass>::Instance;
}

#[no_mangle]
pub extern "C" fn tri_qr_code_new() -> *mut bind::QRCode {
    gtk::init().unwrap(); // FIXME We shouldn't call gtk::init().

    let obj = QRCode::new();
    obj.to_glib_full()
}

#[no_mangle]
pub extern "C" fn tri_qr_code_new_from_bytes(bytes: *mut glib::ffi::GBytes) -> *mut bind::QRCode {
    gtk::init().unwrap(); // FIXME We shouldn't call gtk::init().

    let gbytes: glib::Bytes = unsafe { from_glib_full(bytes) };
    let obj = QRCode::from_bytes(&gbytes);
    obj.to_glib_full()
}

#[no_mangle]
pub extern "C" fn tri_qr_code_get_type() -> glib::ffi::GType {
    gtk::init().unwrap(); // FIXME We shouldn't call gtk::init().

    <QRCode as glib::StaticType>::static_type().into_glib()
}

#[no_mangle]
pub extern "C" fn tri_qr_code_set_bytes(this: *mut bind::QRCode, bytes: *mut glib::ffi::GBytes) {
    let obj: glib::translate::Borrowed<QRCode> = unsafe { from_glib_borrow(this) };
    let gbytes: glib::Bytes = unsafe { from_glib_full(bytes) };
    obj.set_bytes(&gbytes);
}

#[no_mangle]
pub extern "C" fn tri_qr_code_block_size(this: *mut bind::QRCode) -> u32 {
    let obj: glib::translate::Borrowed<QRCode> = unsafe { from_glib_borrow(this) };
    obj.block_size()
}

#[no_mangle]
pub extern "C" fn tri_qr_code_set_block_size(this: *mut bind::QRCode, block_size: u32) {
    let obj: glib::translate::Borrowed<QRCode> = unsafe { from_glib_borrow(this) };
    obj.set_block_size(block_size);
}

#[no_mangle]
pub extern "C" fn tri_qr_code_save_to_png(
    this: *mut bind::QRCode,
    location: *const c_char,
) -> bool {
    let location = unsafe { &*glib::GString::from_glib_borrow(location) };
    let obj: glib::translate::Borrowed<QRCode> = unsafe { from_glib_borrow(this) };
    obj.save_to_png(std::path::Path::new(&location))
        .map_or(false, |_| true)
}
