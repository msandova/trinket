mod notification;
mod notification_bin;
mod progress_icon;
mod qr_code;

#[no_mangle]
pub extern "C" fn tri_init() {
    crate::init();
}
