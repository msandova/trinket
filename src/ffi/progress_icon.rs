use crate::prelude::ProgressIconExt;
use crate::ProgressIcon;

use glib::translate::*;
use gtk::glib;

mod bind {
    use crate::progress_icon::imp;
    use gtk::subclass::prelude::*;

    pub type ProgressIcon = <imp::ProgressIcon as ObjectSubclass>::Instance;
}

#[no_mangle]
pub extern "C" fn tri_progress_icon_new() -> *mut bind::ProgressIcon {
    gtk::init().unwrap(); // FIXME We shouldn't call gtk::init().

    let obj = ProgressIcon::new();
    obj.to_glib_full()
}

#[no_mangle]
pub extern "C" fn tri_progress_icon_get_type() -> glib::ffi::GType {
    gtk::init().unwrap(); // FIXME We shouldn't call gtk::init().

    <ProgressIcon as glib::StaticType>::static_type().into_glib()
}

#[no_mangle]
pub extern "C" fn tri_progress_icon_get_progress(this: *mut bind::ProgressIcon) -> f32 {
    let obj: glib::translate::Borrowed<ProgressIcon> = unsafe { from_glib_borrow(this) };
    obj.progress()
}

#[no_mangle]
pub extern "C" fn tri_progress_icon_set_progress(this: *mut bind::ProgressIcon, progress: f32) {
    let obj: glib::translate::Borrowed<ProgressIcon> = unsafe { from_glib_borrow(this) };
    obj.set_progress(progress);
}

#[no_mangle]
pub extern "C" fn tri_progress_icon_get_inverted(this: *mut bind::ProgressIcon) -> bool {
    let obj: glib::translate::Borrowed<ProgressIcon> = unsafe { from_glib_borrow(this) };
    obj.inverted()
}

#[no_mangle]
pub extern "C" fn tri_progress_icon_set_inverted(this: *mut bind::ProgressIcon, inverted: bool) {
    let obj: glib::translate::Borrowed<ProgressIcon> = unsafe { from_glib_borrow(this) };
    obj.set_inverted(inverted);
}

#[no_mangle]
pub extern "C" fn tri_progress_icon_get_clockwise(this: *mut bind::ProgressIcon) -> bool {
    let obj: glib::translate::Borrowed<ProgressIcon> = unsafe { from_glib_borrow(this) };
    obj.clockwise()
}

#[no_mangle]
pub extern "C" fn tri_progress_icon_set_clockwise(this: *mut bind::ProgressIcon, clockwise: bool) {
    let obj: glib::translate::Borrowed<ProgressIcon> = unsafe { from_glib_borrow(this) };
    obj.set_clockwise(clockwise);
}
