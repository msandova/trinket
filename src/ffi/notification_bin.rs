use crate::prelude::NotificationBinExt;
use crate::Notification;
use crate::NotificationBin;

use glib::translate::*;
use gtk::glib;

mod bind {
    use crate::notification_bin::imp;
    use gtk::subclass::prelude::*;

    pub type NotificationBin = <imp::NotificationBin as ObjectSubclass>::Instance;
    pub type Notification = <crate::notification::imp::Notification as ObjectSubclass>::Instance;
}

#[no_mangle]
pub extern "C" fn tri_notification_bin_new() -> *mut bind::NotificationBin {
    gtk::init().unwrap(); // FIXME We shouldn't call gtk::init().

    let obj = NotificationBin::new();
    obj.to_glib_full()
}

#[no_mangle]
pub extern "C" fn tri_notification_bin_get_type() -> glib::ffi::GType {
    gtk::init().unwrap(); // FIXME We shouldn't call gtk::init().

    <NotificationBin as glib::StaticType>::static_type().into_glib()
}

#[no_mangle]
pub extern "C" fn tri_notification_bin_add_notification(
    this: *mut bind::NotificationBin,
    notification: *mut bind::Notification,
) {
    let obj: glib::translate::Borrowed<NotificationBin> = unsafe { from_glib_borrow(this) };
    let notification: glib::translate::Borrowed<Notification> =
        unsafe { from_glib_borrow(notification) };
    obj.add_notification(&notification);
}

#[no_mangle]
pub extern "C" fn tri_notification_get_queue_size(this: *mut bind::NotificationBin) -> u32 {
    let obj: glib::translate::Borrowed<NotificationBin> = unsafe { from_glib_borrow(this) };
    obj.queue_size()
}

#[no_mangle]
pub extern "C" fn tri_notification_get_child(
    this: *mut bind::NotificationBin,
) -> *mut gtk::ffi::GtkWidget {
    let obj: glib::translate::Borrowed<NotificationBin> = unsafe { from_glib_borrow(this) };

    obj.child().to_glib_full()
}

#[no_mangle]
pub extern "C" fn tri_notification_set_child(
    this: *mut bind::NotificationBin,
    widget: *mut gtk::ffi::GtkWidget,
) {
    let obj: glib::translate::Borrowed<NotificationBin> = unsafe { from_glib_borrow(this) };
    let widget: Option<gtk::Widget> = unsafe { from_glib_none(widget) };
    obj.set_child(widget.as_ref());
}
