use gtk::glib;
use gtk::prelude::*;
use gtk::subclass::prelude::*;

use std::convert::TryFrom;
use std::path::Path;

pub(crate) mod imp {
    use super::*;

    use gtk::{gdk, graphene};

    use once_cell::sync::Lazy;
    use std::cell::{Cell, RefCell};

    #[derive(Debug, Default)]
    pub struct QRCode {
        pub picture: gtk::Picture,
        pub data: RefCell<QRCodeData>,
        pub block_size: Cell<u32>,
        pub bytes: RefCell<Vec<u8>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for QRCode {
        const NAME: &'static str = "TriQRCode";
        type Type = super::QRCode;
        type ParentType = gtk::Widget;

        fn new() -> Self {
            Self {
                block_size: Cell::new(8),
                ..Self::default()
            }
        }
    }

    impl ObjectImpl for QRCode {
        fn constructed(&self, obj: &Self::Type) {
            self.parent_constructed(obj);
            obj.add_css_class("qrcode");
        }

        fn properties() -> &'static [glib::ParamSpec] {
            static PROPERTIES: Lazy<Vec<glib::ParamSpec>> = Lazy::new(|| {
                vec![glib::ParamSpec::new_uint(
                    "block-size",
                    "block-size",
                    "block-size",
                    1,
                    u32::MAX,
                    8,
                    glib::ParamFlags::READWRITE | glib::ParamFlags::EXPLICIT_NOTIFY,
                )]
            });
            PROPERTIES.as_ref()
        }

        fn property(&self, obj: &Self::Type, _id: usize, pspec: &glib::ParamSpec) -> glib::Value {
            match pspec.name() {
                "block-size" => obj.block_size().to_value(),
                _ => unreachable!(),
            }
        }

        fn set_property(
            &self,
            obj: &Self::Type,
            _id: usize,
            value: &glib::Value,
            pspec: &glib::ParamSpec,
        ) {
            match pspec.name() {
                "block-size" => obj.set_block_size(value.get().unwrap()),
                _ => unreachable!(),
            }
        }
    }
    impl WidgetImpl for QRCode {
        fn snapshot(&self, widget: &Self::Type, snapshot: &gtk::Snapshot) {
            let square_width = widget.width() as f32 / self.data.borrow().width as f32;
            let square_height = widget.height() as f32 / self.data.borrow().height as f32;

            self.data
                .borrow()
                .items
                .iter()
                .enumerate()
                .for_each(|(y, line)| {
                    line.iter().enumerate().for_each(|(x, is_dark)| {
                        let color = if *is_dark {
                            widget.style_context().color()
                        } else {
                            widget
                                .style_context()
                                .lookup_color("background")
                                .unwrap_or(gdk::RGBA {
                                    red: 0.0,
                                    blue: 0.0,
                                    green: 0.0,
                                    alpha: 0.0,
                                })
                        };
                        let position = graphene::Rect::new(
                            (x as f32) * square_width,
                            (y as f32) * square_height,
                            square_width,
                            square_height,
                        );

                        snapshot.append_color(&color, &position);
                    });
                });
        }

        fn measure(
            &self,
            widget: &Self::Type,
            orientation: gtk::Orientation,
            for_size: i32,
        ) -> (i32, i32, i32, i32) {
            let self_ = imp::QRCode::from_instance(widget);

            let stride = widget.block_size() as i32;

            let minimum = match orientation {
                gtk::Orientation::Horizontal => self_.data.borrow().width * stride,
                gtk::Orientation::Vertical => self_.data.borrow().height * stride,
                _ => unreachable!(),
            };
            let natural = std::cmp::max(for_size, minimum);
            (minimum, natural, -1, -1)
        }
    }
}

glib::wrapper! {
    /// A widget that display a QR Code.
    ///
    /// The QR code of [`QRCode`] is set with the [QRCodeExt::set_bytes()]
    /// method. It is recommended for a QR Code to have a quiet zone, i.e. a margin of
    /// four times the value of [`QRCodeExt::block_size()`], in most contexts, widgets
    /// already count with such a margin.
    ///
    /// The code can be themed via css, where a recommended quiet-zone
    /// can be as a padding:
    ///
    /// ```css
    /// .qrcode {
    ///     color: black;
    ///     background: white;
    ///     padding: 24px;  /* 4 ⨉ block-size */
    /// }
    /// ```
    ///
    /// **Implements**: [QRCodeExt].
    pub struct QRCode(ObjectSubclass<imp::QRCode>)
        @extends gtk::Widget;
}

pub const NONE_QR_CODE: Option<&QRCode> = None;

impl Default for QRCode {
    fn default() -> Self {
        glib::Object::new(&[]).unwrap()
    }
}

impl QRCode {
    /// Creates a new [`QRCode`].
    pub fn new() -> Self {
        Self::default()
    }

    /// Creates a new [`QRCode`] with a QR code generated from `bytes`.
    pub fn from_bytes(bytes: &[u8]) -> Self {
        let qrcode = Self::default();
        qrcode.set_bytes(bytes);

        qrcode
    }

    fn bytes(&self) -> Vec<u8> {
        let self_ = imp::QRCode::from_instance(self);

        self_.bytes.borrow().to_vec()
    }
}

pub trait QRCodeExt {
    /// Sets the displayed code of `self` to a QR code generated from `bytes`.
    fn set_bytes(&self, bytes: &[u8]);

    /// Gets the block size `self`. This determines the size of the the widget.
    fn block_size(&self) -> u32;

    /// Sets the block size `self`.
    fn set_block_size(&self, block_size: u32);

    /// Saves the displayed QR code to `location`.
    fn save_to_png<P: AsRef<Path>>(&self, location: P) -> Result<(), glib::Error>;

    fn connect_block_size_notify<F: Fn(&Self) + 'static>(&self, f: F) -> glib::SignalHandlerId;
}

impl<W: IsA<QRCode>> QRCodeExt for W {
    fn set_bytes(&self, bytes: &[u8]) {
        let this = imp::QRCode::from_instance(self.as_ref());

        this.bytes.replace(bytes.to_vec());
        let data = QRCodeData::try_from(bytes).unwrap_or_else(|_| {
            glib::g_warning!(None, "Failed to load QRCode from bytes");
            Default::default()
        });
        this.data.replace(data);

        self.as_ref().queue_draw();
        self.as_ref().queue_resize();
    }

    fn block_size(&self) -> u32 {
        let this = imp::QRCode::from_instance(self.as_ref());

        this.block_size.get()
    }

    fn set_block_size(&self, block_size: u32) {
        let this = imp::QRCode::from_instance(self.as_ref());

        this.block_size.set(std::cmp::max(block_size, 1));
        self.notify("block-size");
        self.as_ref().queue_draw();
        self.as_ref().queue_resize();
    }

    fn save_to_png<P: AsRef<Path>>(&self, location: P) -> Result<(), glib::Error> {
        let err = glib::Error::new(glib::FileError::Failed, "Could not store QR code");
        let data = self.as_ref().bytes();
        let code = qrcode::QrCode::new(data).map_err(|_| err.clone())?;
        let image = code.render::<image::Luma<u8>>().build();

        image.save(location).map_err(|_| err)?;

        Ok(())
    }

    fn connect_block_size_notify<F: Fn(&Self) + 'static>(&self, f: F) -> glib::SignalHandlerId {
        self.connect_notify_local(Some("block-size"), move |this, _| {
            f(this);
        })
    }
}

impl Default for QRCodeData {
    fn default() -> Self {
        Self::try_from("".as_bytes()).unwrap()
    }
}

#[derive(Debug, Clone)]
pub struct QRCodeData {
    pub width: i32,
    pub height: i32,
    pub items: Vec<Vec<bool>>,
}

impl TryFrom<&[u8]> for QRCodeData {
    type Error = qrcode::types::QrError;

    fn try_from(data: &[u8]) -> Result<Self, Self::Error> {
        let code = qrcode::QrCode::new(data)?;
        let items = code
            .render::<char>()
            .quiet_zone(false)
            .module_dimensions(1, 1)
            .build()
            .split('\n')
            .into_iter()
            .map(|line| {
                line.chars()
                    .into_iter()
                    .map(|c| !c.is_whitespace())
                    .collect::<Vec<bool>>()
            })
            .collect::<Vec<Vec<bool>>>();

        let height = items.len() as i32;
        let width = items.len() as i32;
        let data = Self {
            width,
            height,
            items,
        };

        Ok(data)
    }
}
