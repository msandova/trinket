#![doc(html_logo_url = "https://msandova.pages.gitlab.gnome.org/trinket/libtrinket.svg")]
//! # Trinket Widget Library
//!
//! Trinket needs to be initialized before use by calling [init()], which should be
//! called after [gtk::init()].
//!
//! To use widget methods import `use trinket::prelude::*;`. For subclassing
//! widgets add `use trinket::subclass::prelude::*;`.
//!
//! See also: [C API Reference](https://msandova.pages.gitlab.gnome.org/trinket/).

use gtk::gdk;
use gtk::prelude::*;
use std::sync::Once;

#[cfg(feature = "bindings")]
pub mod ffi;
pub mod prelude;
pub mod subclass;

mod color_wheel;
mod drop_overlay;
mod notification;
mod notification_bin;
mod notification_widget;
mod progress_icon;
mod qr_code;
mod svg_paintable;

pub use color_wheel::ColorWheel;
pub use drop_overlay::DropOverlay;
pub use notification::{Notification, NONE_NOTIFICATION};
pub use notification_bin::{NotificationBin, NONE_NOTIFICATION_BIN};
pub(crate) use notification_widget::NotificationWidget;
pub use progress_icon::{ProgressIcon, NONE_PROGRESS_ICON};
pub use qr_code::{QRCode, NONE_QR_CODE};
pub use svg_paintable::SvgPaintable;

/// Initializes Trinket.
///
/// Call this function just after initializing GTK, if you are using
/// [`gtk::Application`] it means it must be called when the
/// [`gio::subclass::prelude::ApplicationImpl::startup()`](gtk::gio::subclass::prelude::ApplicationImpl::startup()) signal is emitted.
///
/// If Trinket has already been initialized, the function will simply return.
///
/// This makes sure translations, and types for the Trinket
/// library are set up properly.
#[doc(alias = "tri_init")]
pub fn init() {
    static INIT: Once = Once::new();

    INIT.call_once(|| {
        ColorWheel::static_type();
        DropOverlay::static_type();
        NotificationBin::static_type();
        ProgressIcon::static_type();
        QRCode::static_type();
        SvgPaintable::static_type();

        gtk::init().unwrap();
        let provider = gtk::CssProvider::new();
        provider.load_from_data(include_bytes!("style.css"));
        if let Some(display) = gdk::Display::default() {
            gtk::StyleContext::add_provider_for_display(
                &display,
                &provider,
                gtk::STYLE_PROVIDER_PRIORITY_THEME,
            );
        }
    });
}
