use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::{gdk, glib, graphene, gsk};

use std::f32::consts::PI;

#[derive(Debug, Copy, Clone)]
pub(crate) struct HSV {
    h: f32,
    s: f32,
    v: f32,
}

#[derive(Debug, Copy, Clone)]
pub(crate) enum Drag {
    Ring(f32, f32),
    Triangle(f32, f32),
    None,
}

impl From<gdk::RGBA> for HSV {
    fn from(color: gdk::RGBA) -> Self {
        let (h, s, v) = gtk::rgb_to_hsv(color.red, color.green, color.blue);

        HSV { h, s, v }
    }
}

impl From<HSV> for gdk::RGBA {
    fn from(color: HSV) -> Self {
        let (red, green, blue) = gtk::hsv_to_rgb(color.h, color.s, color.v);

        gdk::RGBA {
            red,
            green,
            blue,
            alpha: 1.0,
        }
    }
}

impl HSV {
    pub fn as_int(&self) -> (i32, i32, i32) {
        (
            (self.h * 359.0 + 1.0) as i32,
            (self.s * 99.0 + 1.0) as i32,
            (self.v * 99.0 + 1.0) as i32,
        )
    }

    pub fn from_int(h: i32, s: i32, v: i32) -> Self {
        Self {
            h: (h as f32 - 1.0) / 359.0,
            s: (s as f32 - 1.0) / 99.0,
            v: (v as f32 - 1.0) / 99.0,
        }
    }
}

mod imp {
    use once_cell::sync::{Lazy, OnceCell};
    use std::cell::Cell;

    use super::*;

    #[derive(Debug)]
    pub struct ColorWheel {
        pub(crate) hsv: Cell<HSV>,
        pub(crate) state: Cell<Drag>,
        pub(crate) ring_texture: OnceCell<gdk::MemoryTexture>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ColorWheel {
        const NAME: &'static str = "TriColorWheel";
        type Type = super::ColorWheel;
        type ParentType = gtk::Widget;

        fn class_init(klass: &mut Self::Class) {
            klass.set_layout_manager_type::<gtk::BinLayout>();
        }

        fn new() -> Self {
            Self {
                hsv: Cell::new(HSV {
                    h: 0.0,
                    s: 1.0,
                    v: 1.0,
                }),
                state: Cell::new(Drag::None),
                ring_texture: Default::default(),
            }
        }
    }

    impl ObjectImpl for ColorWheel {
        fn properties() -> &'static [glib::ParamSpec] {
            static PROPERTIES: Lazy<Vec<glib::ParamSpec>> = Lazy::new(|| {
                vec![
                    glib::ParamSpec::new_int("h", "h", "h", 1, 360, 1, glib::ParamFlags::READWRITE),
                    glib::ParamSpec::new_int(
                        "s",
                        "s",
                        "s",
                        1,
                        100,
                        100,
                        glib::ParamFlags::READWRITE,
                    ),
                    glib::ParamSpec::new_int(
                        "v",
                        "v",
                        "v",
                        1,
                        100,
                        100,
                        glib::ParamFlags::READWRITE,
                    ),
                    glib::ParamSpec::new_boxed(
                        "rgba",
                        "rgba",
                        "rgba",
                        gdk::RGBA::static_type(),
                        glib::ParamFlags::READWRITE,
                    ),
                ]
            });
            PROPERTIES.as_ref()
        }

        fn property(&self, _obj: &Self::Type, _id: usize, pspec: &glib::ParamSpec) -> glib::Value {
            match pspec.name() {
                "h" => self.hsv.get().as_int().0.to_value(),
                "s" => self.hsv.get().as_int().1.to_value(),
                "v" => self.hsv.get().as_int().2.to_value(),
                "rgba" => gdk::RGBA::from(self.hsv.get()).to_value(),
                _ => unimplemented!(),
            }
        }

        fn set_property(
            &self,
            obj: &Self::Type,
            _id: usize,
            value: &glib::Value,
            pspec: &glib::ParamSpec,
        ) {
            match pspec.name() {
                "h" => {
                    let (_, s, v) = self.hsv.get().as_int();
                    let h = value.get().unwrap();
                    self.hsv.set(HSV::from_int(h, s, v));
                    obj.notify("rgba");
                    obj.queue_draw();
                }
                "s" => {
                    let (h, _, v) = self.hsv.get().as_int();
                    let s = value.get().unwrap();
                    self.hsv.set(HSV::from_int(h, s, v));
                    obj.notify("rgba");
                    obj.queue_draw();
                }
                "v" => {
                    let (h, s, _) = self.hsv.get().as_int();
                    let v = value.get().unwrap();
                    self.hsv.set(HSV::from_int(h, s, v));
                    obj.notify("rgba");
                    obj.queue_draw();
                }
                "rgba" => {
                    let color = value.get::<gdk::RGBA>().unwrap();

                    self.hsv.set(HSV::from(color));
                    obj.notify("h");
                    obj.notify("s");
                    obj.notify("v");
                    obj.queue_draw();
                }
                _ => unimplemented!(),
            };
        }
        fn constructed(&self, obj: &Self::Type) {
            self.parent_constructed(obj);

            let click = gtk::GestureClick::new();
            click.set_button(gdk::BUTTON_PRIMARY);

            click.connect_pressed(glib::clone!(@weak obj => move |_, _, x, y| {
                let (x, y) = (x as f32, y as f32);
                if obj.is_in_ring(x , y) {
                    let imp = imp::ColorWheel::from_instance(&obj);

                    let mut hsv = imp.hsv.get();
                    hsv.h = obj.h_from_ring_pos(x, y);

                    obj.set_property("rgba", gdk::RGBA::from(hsv)).unwrap();
                    return
                }

                if obj.is_in_triangle(x,y) {
                    let hsv = obj.hsv_from_triangle_pos(x,y);
                    obj.set_property("rgba", gdk::RGBA::from(hsv)).unwrap();
                }
            }));

            obj.add_controller(&click);

            let drag = gtk::GestureDrag::new();

            drag.connect_drag_begin(glib::clone!(@weak obj => move |_, x, y| {
                let (x, y) = (x as f32, y as f32);
                if obj.is_in_ring(x , y) {
                    let imp = imp::ColorWheel::from_instance(&obj);
                    imp.state.set(Drag::Ring(x,y));

                    return
                }

                if obj.is_in_triangle(x,y) {
                    let imp = imp::ColorWheel::from_instance(&obj);
                    imp.state.set(Drag::Triangle(x,y));

                    return
                }
            }));

            drag.connect_drag_end(glib::clone!(@weak obj => move |_, _x, _y| {
                let imp = imp::ColorWheel::from_instance(&obj);
                imp.state.set(Drag::None);
            }));

            drag.connect_drag_update(glib::clone!(@weak obj => move |_, x, y| {
                let (x, y) = (x as f32, y as f32);
                let imp = imp::ColorWheel::from_instance(&obj);
                let state = imp.state.get();

                match state {
                    Drag::Ring(x_0,y_0) => {
                        let mut hsv = imp.hsv.get();
                        hsv.h = obj.h_from_ring_pos(x + x_0, y+y_0);

                        obj.set_property("rgba", gdk::RGBA::from(hsv)).unwrap();
                    },
                    Drag::Triangle(x_0, y_0) => {
                        let hsv = obj.hsv_from_triangle_pos(x+x_0,y+y_0);
                        obj.set_property("rgba", gdk::RGBA::from(hsv)).unwrap();
                    },
                    Drag::None => (),
                }
            }));

            obj.add_controller(&drag);
        }
    }
    impl WidgetImpl for ColorWheel {
        fn snapshot(&self, widget: &Self::Type, snapshot: &gtk::Snapshot) {
            self.parent_snapshot(widget, snapshot);

            widget.snapshot_triangle(snapshot);
            widget.snapshot_triangle_indicator(snapshot);

            widget.snapshot_ring(snapshot);
            widget.snapshot_ring_indicator(snapshot);
        }
    }
}

glib::wrapper! {
    pub struct ColorWheel(ObjectSubclass<imp::ColorWheel>)
        @extends gtk::Widget;
}

impl Default for ColorWheel {
    fn default() -> Self {
        glib::Object::new(&[]).unwrap()
    }
}

impl ColorWheel {
    fn snapshot_ring(&self, snapshot: &gtk::Snapshot) {
        let imp = imp::ColorWheel::from_instance(self);
        let width = self.size();
        let height = self.size();

        if let Some(texture) = imp.ring_texture.get() {
            texture.snapshot(snapshot.upcast_ref(), width as f64, height as f64);
        } else {
            let mut bytes: Vec<u8> = vec![];
            let center_x = width / 2;
            let center_y = height / 2;

            let outer = width / 2;
            let inner = outer - self.ring_diameter();

            for y in 0..height {
                let dy = -(y - center_y);
                for x in 0..width {
                    let dx = x - center_x;

                    let dist = dx * dx + dy * dy;
                    if dist < inner * inner || dist > outer * outer {
                        bytes.extend_from_slice(&[0, 0, 0, 0]);
                        continue;
                    }

                    let mut angle = (dy as f32).atan2(dx as f32);
                    if angle < 0.0 {
                        angle += 2.0 * PI;
                    }
                    let h = angle / (2.0 * PI);
                    let rgb = gdk::RGBA::from(HSV { h, s: 1.0, v: 1.0 });

                    let pixel = [
                        (rgb.red * 255.0) as u8,
                        (rgb.green * 255.0) as u8,
                        (rgb.blue * 255.0) as u8,
                        255_u8,
                    ];
                    bytes.extend_from_slice(&pixel);
                }
            }

            let gbytes = glib::Bytes::from_owned(bytes);
            let format = gdk::MemoryFormat::R8g8b8a8;
            let stride = (width * 4) as usize;
            let texture = gdk::MemoryTexture::new(width, height, format, &gbytes, stride);

            texture.snapshot(snapshot.upcast_ref(), width as f64, height as f64);

            imp.ring_texture.set(texture).unwrap();
        }
    }

    // FIXME: Borked.
    fn snapshot_triangle_indicator(&self, snapshot: &gtk::Snapshot) {
        let imp = imp::ColorWheel::from_instance(self);

        let triangle = self.triangle();
        let hsv = imp.hsv.get();
        let (h_vert, s_vert, v_vert) = triangle.points();

        let (h, s, v) = (hsv.h, hsv.s, hsv.v);

        let inverse_h = match h + 0.5 <= 1.0 {
            true => h + 0.5,
            false => h - 0.5,
        };

        let u = 1.0 - s;
        let v = 1.0 - v;

        let a = h_vert.to_vec3().scale(1.0 - u - v);
        let b = s_vert.to_vec3().scale(u);
        let c = v_vert.to_vec3().scale(v);

        let p = a.add(&b).add(&c);

        let radius = 5.0;
        let rect = graphene::Rect::new(p.x() - radius, p.y() - radius, 2.0 * radius, 2.0 * radius);
        let round = gsk::RoundedRect::from_rect(rect.clone(), radius);

        let inverse_color = gdk::RGBA::from(HSV {
            h: inverse_h,
            v: s,
            s: v,
        });

        snapshot.push_rounded_clip(&round);
        snapshot.append_color(&inverse_color, &rect);
        snapshot.pop();
    }

    fn snapshot_ring_indicator(&self, snapshot: &gtk::Snapshot) {
        let imp = imp::ColorWheel::from_instance(self);
        let h = imp.hsv.get().h;

        let angle = h * 360.0;

        let size = self.size() as f32;

        let center_x = size / 2.0;
        let center_y = size / 2.0;

        let outer = size / 2.0;
        let inner = outer - self.ring_diameter() as f32;

        let rect = graphene::Rect::new(inner, 0.0, self.ring_diameter() as f32, 2.0);

        let inverse_h = match h + 0.5 <= 1.0 {
            true => h + 0.5,
            false => h - 0.5,
        };
        let inverse_color = gdk::RGBA::from(HSV {
            h: inverse_h,
            s: 1.0,
            v: 1.0,
        });
        let center = graphene::Point::new(center_x, center_y);
        let minus_center = graphene::Point::new(-center_x, -center_y);

        snapshot.translate(&center);
        snapshot.rotate(360.0 - angle);

        snapshot.append_color(&inverse_color, &rect);

        snapshot.rotate(angle);
        snapshot.translate(&minus_center);
    }

    fn snapshot_triangle(&self, snapshot: &gtk::Snapshot) {
        let mut bytes: Vec<u8> = vec![];
        let width = self.size();
        let height = self.size();

        for y in 0..height {
            for x in 0..width {
                if !self.is_in_triangle(x as f32, y as f32) {
                    bytes.extend_from_slice(&[0, 0, 0, 0]);
                    continue;
                }

                let hsv = self.hsv_from_triangle_pos(x as f32, y as f32);
                let rgb = gdk::RGBA::from(hsv);

                let pixel = [
                    (rgb.red * 255.0) as u8,
                    (rgb.green * 255.0) as u8,
                    (rgb.blue * 255.0) as u8,
                    255_u8,
                ];
                bytes.extend_from_slice(&pixel);
            }
        }

        let gbytes = glib::Bytes::from_owned(bytes);
        let format = gdk::MemoryFormat::R8g8b8a8;
        let stride = (width * 4) as usize;
        let texture = gdk::MemoryTexture::new(width, height, format, &gbytes, stride);

        texture.snapshot(snapshot.upcast_ref(), width as f64, height as f64);
    }

    fn size(&self) -> i32 {
        256
    }

    fn ring_diameter(&self) -> i32 {
        24
    }

    fn is_in_ring(&self, x: f32, y: f32) -> bool {
        let size = self.size() as f32;
        let center_x = size / 2.0;
        let center_y = size / 2.0;

        let outer = size / 2.0;
        let inner = outer - self.ring_diameter() as f32;

        let dx = x - center_x;
        let dy = y - center_y;
        let dist = dx * dx + dy * dy;

        dist >= inner * inner && dist <= outer * outer
    }

    fn is_in_triangle(&self, x: f32, y: f32) -> bool {
        let triangle = self.triangle();
        let point = graphene::Point3D::new(x, y, 0.0);

        triangle.contains_point(&point)
    }

    // FIXME: Borked.
    fn hsv_from_triangle_pos(&self, x: f32, y: f32) -> HSV {
        let imp = imp::ColorWheel::from_instance(self);
        let triangle = self.triangle();
        let point = graphene::Point3D::new(x, y, 0.0);

        let uv = triangle.barycoords(Some(&point)).unwrap();

        // Inverted for some reason.
        let u = uv.y();
        let v = uv.x();

        let h = imp.hsv.get().h;

        let s = 1.0 - u;
        let v = 1.0 - v;

        HSV { h, s, v }
    }

    fn h_from_ring_pos(&self, x: f32, y: f32) -> f32 {
        let width = self.size() as f32;
        let height = self.size() as f32;

        let center_x = width / 2.0;
        let center_y = height / 2.0;

        let dy = -(y - center_y);
        let dx = x - center_x;

        if (dx - dy).abs() <= std::f32::EPSILON {
            return 0.0;
        }

        let mut angle = dy.atan2(dx);
        if angle < 0.0 {
            angle += 2.0 * PI;
        }

        angle / (2.0 * PI)
    }

    fn triangle(&self) -> graphene::Triangle {
        let imp = imp::ColorWheel::from_instance(self);

        let size = self.size() as f32;
        let center_x = size / 2.0;
        let center_y = size / 2.0;

        let outer = size / 2.0;
        let inner = outer - self.ring_diameter() as f32;

        let angle = imp.hsv.get().h * 2.0 * PI;

        let hx = center_x + angle.cos() * inner;
        let hy = center_y - angle.sin() * inner;
        let sx = center_x + (angle + 2.0 * PI / 3.0).cos() * inner;
        let sy = center_y - (angle + 2.0 * PI / 3.0).sin() * inner;
        let vx = center_x + (angle + 4.0 * PI / 3.0).cos() * inner;
        let vy = center_y - (angle + 4.0 * PI / 3.0).sin() * inner;

        let h = graphene::Point3D::new(hx, hy, 0.0);
        let s = graphene::Point3D::new(sx, sy, 0.0);
        let v = graphene::Point3D::new(vx, vy, 0.0);

        graphene::Triangle::from_point3d(Some(&h), Some(&s), Some(&v))
    }
}
