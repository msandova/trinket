use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::{gio, glib};

pub(crate) mod imp {
    use super::*;

    use gtk::glib::value::FromValue;
    use gtk::glib::value::ValueTypeChecker;
    use gtk::CompositeTemplate;
    use once_cell::sync::Lazy;
    use std::time::Duration;

    const BETWEEN_NOTIFICATIONS: Duration = Duration::from_millis(300);

    #[derive(Debug, CompositeTemplate)]
    #[template(file = "notification_bin.ui")]
    pub struct NotificationBin {
        #[template_child]
        pub revealer: TemplateChild<gtk::Revealer>,
        #[template_child]
        pub overlay: TemplateChild<gtk::Overlay>,

        pub queue: gio::ListStore,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for NotificationBin {
        const NAME: &'static str = "TriNotificationBin";
        type Type = super::NotificationBin;
        type ParentType = gtk::Widget;
        type Interfaces = (gtk::Buildable,);

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
            klass.set_layout_manager_type::<gtk::BinLayout>();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }

        fn new() -> Self {
            let queue = gio::ListStore::new(crate::Notification::static_type());

            Self {
                queue,
                revealer: TemplateChild::default(),
                overlay: TemplateChild::default(),
            }
        }
    }

    impl ObjectImpl for NotificationBin {
        fn properties() -> &'static [glib::ParamSpec] {
            static PROPERTIES: Lazy<Vec<glib::ParamSpec>> = Lazy::new(|| {
                vec![
                    glib::ParamSpec::new_uint(
                        "queue-size",
                        "queue-size",
                        "queue-size",
                        0,
                        u32::MAX,
                        0,
                        glib::ParamFlags::READABLE,
                    ),
                    glib::ParamSpec::new_object(
                        "child",
                        "Child",
                        "Child Widget",
                        gtk::Widget::static_type(),
                        glib::ParamFlags::READWRITE
                            | glib::ParamFlags::EXPLICIT_NOTIFY
                            | glib::ParamFlags::CONSTRUCT,
                    ),
                ]
            });
            PROPERTIES.as_ref()
        }

        fn property(&self, obj: &Self::Type, _id: usize, pspec: &glib::ParamSpec) -> glib::Value {
            match pspec.name() {
                "queue-size" => obj.queue_size().to_value(),
                "child" => obj.child().to_value(),
                _ => unreachable!(),
            }
        }

        fn set_property(
            &self,
            obj: &Self::Type,
            _id: usize,
            value: &glib::Value,
            pspec: &glib::ParamSpec,
        ) {
            match pspec.name() {
                "child" => unsafe {
                    if <gtk::Widget as FromValue>::Checker::check(value.get().unwrap()).is_ok() {
                        obj.set_child(Some(&gtk::Widget::from_value(value.get().unwrap())));
                    } else {
                        obj.set_child(gtk::NONE_WIDGET);
                    }
                },
                _ => unreachable!(),
            }
        }

        fn constructed(&self, obj: &Self::Type) {
            self.parent_constructed(obj);
            self.revealer.connect_child_revealed_notify(glib::clone!(@weak obj => move |revealer| {
                if !revealer.is_child_revealed() {
                    revealer.set_child(gtk::NONE_WIDGET);
                    glib::timeout_add_local(BETWEEN_NOTIFICATIONS, glib::clone!(@weak obj => @default-return glib::Continue(false), move || {
                        let self_ = imp::NotificationBin::from_instance(&obj);

                        // Return if the user added a notification in the downtime.
                        // This might be racy.
                        if obj.is_displaying() {
                            return glib::Continue(false)
                        }

                        if let Some(notification) = self_.queue.item(0) {
                            let notification = notification.downcast::<crate::Notification>().unwrap();

                            self_.queue.remove(0);
                            obj.notify("queue-size");

                            obj.popup(&notification);
                        }

                        glib::Continue(false)
                    }));
                }
            }));
        }

        fn dispose(&self, _obj: &Self::Type) {
            self.overlay.unparent();
        }
    }
    impl WidgetImpl for NotificationBin {}
    impl BuildableImpl for NotificationBin {
        fn add_child(
            &self,
            buildable: &Self::Type,
            builder: &gtk::Builder,
            child: &glib::Object,
            type_: Option<&str>,
        ) {
            // We first check if the main child `overlay` has already been bound.
            // TODO use `if !self.overlay.is_bound()` once it lands in gtk4-rs.
            if child.is::<gtk::Widget>() {
                if buildable.first_child().is_none() {
                    self.parent_add_child(buildable, builder, child, type_);
                } else {
                    buildable.set_child(child.downcast_ref::<gtk::Widget>());
                }
            } else {
                self.parent_add_child(buildable, builder, child, type_);
            };
        }
    }
}

glib::wrapper! {
    /// An overlay that displays [`crate::Notification`]s and the content for a window.
    ///
    /// Notifications are displayed with [`NotificationBinExt::add_notification()`]. [`NotificationBin`] implementes a queue of
    /// for notifications, each notification is displayed for five seconds and then the next one is displayed. [`NotificationBin`] is
    /// intended to be a direct child of a subclass of [`gtk::Window`] and the content for window can be set with
    /// the [`NotificationBinExt::child()`] property as follows:
    ///
    /// ```xml
    /// <object class="GtkWindow">
    ///   <child>
    ///     <object class="TriNotificationBin" id="notification_bin"/>
    ///       <property name="child">
    ///         <!-- Rest of the widget tree. -->
    ///       </property>
    ///   </child>
    /// </object>
    /// ```
    ///
    /// **Implements**: [`NotificationBinExt`]
    #[deprecated(note = "please use `AdwToastOverlay` instead")]
    pub struct NotificationBin(ObjectSubclass<imp::NotificationBin>)
        @extends gtk::Widget, @implements gtk::Buildable;
}

pub const NONE_NOTIFICATION_BIN: Option<&NotificationBin> = None;

impl Default for NotificationBin {
    fn default() -> Self {
        glib::Object::new(&[]).unwrap()
    }
}

impl NotificationBin {
    /// Creates a new [`NotificationBin`].
    pub fn new() -> Self {
        Self::default()
    }

    fn popup(&self, notification: &crate::Notification) {
        let self_ = imp::NotificationBin::from_instance(self);
        let widget = crate::NotificationWidget::from_notification(notification);

        widget
            .connect_local(
                "close",
                false,
                glib::clone!(@weak self as obj => @default-return None, move |_| {
                    obj.popdown();

                    None
                }),
            )
            .unwrap();

        self_.revealer.set_child(Some(&widget));
        self_.revealer.set_reveal_child(true);
    }

    fn popdown(&self) {
        let self_ = imp::NotificationBin::from_instance(self);

        self_.revealer.set_reveal_child(false);
    }

    fn is_displaying(&self) -> bool {
        let self_ = imp::NotificationBin::from_instance(self);

        self_.revealer.child().is_some()
    }
}

pub trait NotificationBinExt {
    /// Displays a `notification` in `self`. If `self` is already displaying a notificaiton,
    /// it will instead be added to the queue and be displayed later.
    fn add_notification(&self, notification: &crate::Notification);

    /// Gets size of the notification queue of `self`.
    fn queue_size(&self) -> u32;

    /// Gets the child widget of `self`.
    fn child(&self) -> Option<gtk::Widget>;
    /// Sets the child widget of `self`.
    fn set_child(&self, widget: Option<&gtk::Widget>);

    fn connect_queue_size_notify<F: Fn(&Self) + 'static>(&self, f: F) -> glib::SignalHandlerId;
    fn connect_child_notify<F: Fn(&Self) + 'static>(&self, f: F) -> glib::SignalHandlerId;
}

impl<W: IsA<NotificationBin>> NotificationBinExt for W {
    fn add_notification(&self, notification: &crate::Notification) {
        let self_ = imp::NotificationBin::from_instance(self.as_ref());

        if !self.as_ref().is_displaying() {
            self.as_ref().popup(notification);
        } else {
            self_.queue.append(notification);
            self.notify("queue-size");
        }
    }

    fn queue_size(&self) -> u32 {
        let self_ = imp::NotificationBin::from_instance(self.as_ref());

        self_.queue.n_items()
    }

    fn child(&self) -> Option<gtk::Widget> {
        let self_ = imp::NotificationBin::from_instance(self.as_ref());

        self_.overlay.child()
    }

    fn set_child(&self, widget: Option<&gtk::Widget>) {
        let self_ = imp::NotificationBin::from_instance(self.as_ref());

        self_.overlay.set_child(widget);
    }

    fn connect_queue_size_notify<F: Fn(&Self) + 'static>(&self, f: F) -> glib::SignalHandlerId {
        self.connect_notify_local(Some("queue-size"), move |this, _| {
            f(this);
        })
    }

    fn connect_child_notify<F: Fn(&Self) + 'static>(&self, f: F) -> glib::SignalHandlerId {
        self.connect_notify_local(Some("child"), move |this, _| {
            f(this);
        })
    }
}
