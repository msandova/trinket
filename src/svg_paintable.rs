use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::{cairo, gdk, gio, glib, graphene};

use std::cell::Cell;

mod imp {
    use super::*;

    use once_cell::sync::Lazy;
    use once_cell::sync::OnceCell;

    #[derive(Default)]
    pub struct SvgPaintable {
        pub handle: OnceCell<rsvg::SvgHandle>,
        pub scale_factor: Cell<i32>,
        pub width: Cell<i32>,
        pub height: Cell<i32>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for SvgPaintable {
        const NAME: &'static str = "SvgPaintable";
        type Type = super::SvgPaintable;
        type ParentType = glib::Object;
        type Interfaces = (gdk::Paintable,);

        fn new() -> Self {
            Self {
                handle: OnceCell::new(),
                scale_factor: Cell::new(1),
                width: Cell::new(0),
                height: Cell::new(0),
            }
        }
    }

    impl ObjectImpl for SvgPaintable {
        fn properties() -> &'static [glib::ParamSpec] {
            static PROPERTIES: Lazy<Vec<glib::ParamSpec>> = Lazy::new(|| {
                vec![glib::ParamSpec::new_int(
                    "scale-factor",
                    "scale-factor",
                    "scale-factor",
                    0,
                    i32::MAX,
                    1,
                    glib::ParamFlags::READWRITE,
                )]
            });
            PROPERTIES.as_ref()
        }

        fn property(&self, _obj: &Self::Type, _id: usize, pspec: &glib::ParamSpec) -> glib::Value {
            match pspec.name() {
                "scale-factor" => self.scale_factor.get().to_value(),
                _ => unimplemented!(),
            }
        }

        fn set_property(
            &self,
            obj: &Self::Type,
            _id: usize,
            value: &glib::Value,
            pspec: &glib::ParamSpec,
        ) {
            match pspec.name() {
                "scale-factor" => {
                    self.scale_factor.set(value.get().unwrap());
                    obj.invalidate_contents();
                }
                _ => unimplemented!(),
            };
        }
    }
    impl PaintableImpl for SvgPaintable {
        fn snapshot(
            &self,
            paintable: &Self::Type,
            snapshot: &gdk::Snapshot,
            width: f64,
            height: f64,
        ) {
            let snapshot = snapshot.downcast_ref::<gtk::Snapshot>().unwrap();
            let rect = graphene::Rect::new(0.0, 0.0, width as f32, height as f32);
            let cr = snapshot.append_cairo(&rect).unwrap();

            paintable
                .render(&cr, width as i32, height as i32)
                .unwrap_or_else(|err| log::debug!("Failed to render svg: {:?}", err));
        }

        fn intrinsic_width(&self, _paintable: &Self::Type) -> i32 {
            let handle = self.handle.get().unwrap();
            let renderer = rsvg::CairoRenderer::new(&handle);

            match renderer.intrinsic_dimensions().width {
                Some(width) => width.length as i32,
                None => 0,
            }
        }

        fn intrinsic_height(&self, _paintable: &Self::Type) -> i32 {
            let renderer = rsvg::CairoRenderer::new(&self.handle.get().unwrap());

            match renderer.intrinsic_dimensions().height {
                Some(width) => width.length as i32,
                None => 0,
            }
        }
    }
}

glib::wrapper! {
    pub struct SvgPaintable(ObjectSubclass<imp::SvgPaintable>)
        @implements gdk::Paintable;
}

impl SvgPaintable {
    pub fn new(bytes: &glib::Bytes) -> Self {
        let paintable: Self = glib::Object::new(&[]).unwrap();
        let paintable_ = imp::SvgPaintable::from_instance(&paintable);

        let stream = gio::MemoryInputStream::from_bytes(&bytes);
        let handle = rsvg::Loader::new()
            .read_stream(&stream, gio::NONE_FILE, gio::NONE_CANCELLABLE)
            .unwrap();

        paintable_
            .handle
            .set(handle)
            .unwrap_or_else(|_| log::debug!("Could not set handle"));
        paintable
    }

    pub fn render(&self, cr: &cairo::Context, width: i32, height: i32) -> anyhow::Result<()> {
        let self_ = imp::SvgPaintable::from_instance(self);
        let renderer = rsvg::CairoRenderer::new(&self_.handle.get().unwrap());

        // FIXME Remove if https://gitlab.gnome.org/GNOME/librsvg/-/issues/826 is fixed
        let scale = self_.scale_factor.get();
        let surface =
            cairo::ImageSurface::create(cairo::Format::ARgb32, scale * width, scale * height)?;

        let cr2 = cairo::Context::new(&surface)?;

        renderer.render_document(
            &cr2,
            &cairo::Rectangle {
                x: 0.0,
                y: 0.0,
                width: (scale * width) as f64,
                height: (scale * height) as f64,
            },
        )?;
        cr.scale(1.0 / scale as f64, 1.0 / scale as f64);
        cr.set_source_surface(&surface, 0.0, 0.0)?;
        cr.paint()?;

        Ok(())
    }
}
