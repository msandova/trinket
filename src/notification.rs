use gtk::subclass::prelude::*;
use gtk::{gio, glib};

pub(crate) mod imp {
    use super::*;

    use std::cell::RefCell;

    #[derive(Debug, Default)]
    pub struct Notification {
        pub title: RefCell<Option<String>>,
        pub button_label: RefCell<Option<String>>,
        pub action_name: RefCell<Option<String>>,
        pub action_target: RefCell<Option<glib::Variant>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Notification {
        const NAME: &'static str = "TriNotification";
        type Type = super::Notification;
        type ParentType = glib::Object;
    }

    impl ObjectImpl for Notification {}
}

glib::wrapper! {
    /// A notification object to be consumed by [crate::NotificationBin].
    ///
    /// To display a notification with an action button either [Notification::set_button()] or
    /// [Notification::set_button_with_target_value()] can be used.
    pub struct Notification(ObjectSubclass<imp::Notification>);
}

pub const NONE_NOTIFICATION: Option<&Notification> = None;

impl Default for Notification {
    fn default() -> Self {
        glib::Object::new(&[]).unwrap()
    }
}

#[deprecated(note = "please use `AdwToast` instead")]
impl Notification {
    /// `title`: the title to be displayed
    ///
    /// Creates a new [`Notification`] which will display `title` as its text.
    pub fn new(title: &str) -> Self {
        let notification = Self::default();
        let notification_ = imp::Notification::from_instance(&notification);

        notification_.title.replace(Some(title.to_string()));

        notification
    }

    /// Adds a button to self that activates the action in detailed_action when clicked.
    /// That action must be an application-wide action (starting with “app.”).
    /// If detailed_action contains a target, the action will be activated with that target as its parameter.
    /// See gio::Action::parse_detailed_name() for a description of the format for `detailed_action`.
    pub fn set_button(&self, label: &str, detailed_action: &str) {
        // TODO This should return an error?
        let self_ = imp::Notification::from_instance(self);

        if let Ok((action_name, action_target)) = gio::Action::parse_detailed_name(detailed_action)
        {
            self_.button_label.replace(Some(label.to_string()));
            self_.action_name.replace(Some(action_name.to_string()));
            self_.action_target.replace(action_target.as_variant());
        }
    }

    /// `label`: The button label
    /// `action`: The action name
    /// `target`: target to use for `action`'s parameter, or `None`
    ///
    /// Sets a button to be displayed of `self` that activates the `action` when clicked.
    /// If `target` is not `None`, `action` will be activated with `target` as its parameter.
    pub fn set_button_with_target_value(
        &self,
        label: &str,
        action: &str,
        target: Option<&glib::Variant>,
    ) {
        let self_ = imp::Notification::from_instance(self);
        self_.action_name.replace(Some(action.to_string()));
        self_.action_target.replace(target.cloned());
        self_.button_label.replace(Some(label.to_string()));
    }

    pub(crate) fn title(&self) -> String {
        let self_ = imp::Notification::from_instance(self);
        self_
            .title
            .borrow()
            .as_ref()
            .unwrap_or(&"".to_string())
            .to_string()
    }

    pub(crate) fn action_name(&self) -> Option<String> {
        let self_ = imp::Notification::from_instance(self);
        self_.action_name.borrow().clone()
    }

    pub(crate) fn button_label(&self) -> Option<String> {
        let self_ = imp::Notification::from_instance(self);
        self_.button_label.borrow().clone()
    }

    pub(crate) fn action_target(&self) -> Option<glib::Variant> {
        let self_ = imp::Notification::from_instance(self);
        self_.action_target.borrow().clone()
    }
}
