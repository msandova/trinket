use gtk::glib;
use gtk::subclass::prelude::*;
use gtk::{prelude::*, CompositeTemplate};

mod imp {
    use once_cell::sync::Lazy;
    use std::cell::RefCell;

    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(file = "notification_widget.ui")]
    pub struct NotificationWidget {
        pub source: RefCell<Option<glib::SourceId>>,

        #[template_child]
        box_: TemplateChild<gtk::Box>,
        #[template_child]
        pub action_button: TemplateChild<gtk::Button>,
        #[template_child]
        pub close_button: TemplateChild<gtk::Button>,
        #[template_child]
        pub title: TemplateChild<gtk::Label>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for NotificationWidget {
        const NAME: &'static str = "TriNotificationWidget";
        type Type = super::NotificationWidget;
        type ParentType = gtk::Widget;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
            klass.set_layout_manager_type::<gtk::BinLayout>();
            klass.install_action("close", None, move |obj, _, _| {
                obj.close();
            });
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for NotificationWidget {
        fn constructed(&self, obj: &Self::Type) {
            self.parent_constructed(obj);

            self.action_button
                .connect_clicked(glib::clone!(@weak obj => move |_| {
                    obj.close();
                }));
        }
        fn dispose(&self, _obj: &Self::Type) {
            if let Some(source) = self.source.borrow_mut().take() {
                glib::source_remove(source);
            };
            self.box_.unparent();
        }

        fn signals() -> &'static [glib::subclass::Signal] {
            static SIGNALS: Lazy<Vec<glib::subclass::Signal>> = Lazy::new(|| {
                vec![glib::subclass::Signal::builder("close", &[], glib::Type::UNIT.into()).build()]
            });
            SIGNALS.as_ref()
        }
    }
    impl WidgetImpl for NotificationWidget {
        fn realize(&self, widget: &Self::Type) {
            self.parent_realize(widget);
            const DURATION: u32 = 5;

            let source = glib::timeout_add_seconds_local(
                DURATION,
                glib::clone!(@weak widget => @default-return glib::Continue(false), move || {
                    widget.close();

                    glib::Continue(false)
                }),
            );

            self.source.replace(Some(source));
        }
    }
}

glib::wrapper! {
    pub struct NotificationWidget(ObjectSubclass<imp::NotificationWidget>)
        @extends gtk::Widget;
}

impl Default for NotificationWidget {
    fn default() -> Self {
        glib::Object::new(&[]).unwrap()
    }
}

impl NotificationWidget {
    pub(crate) fn from_notification(notification: &crate::Notification) -> Self {
        let widget = Self::default();
        let widget_ = imp::NotificationWidget::from_instance(&widget);

        widget_.title.set_label(&notification.title());
        if let Some(label) = notification.button_label() {
            widget_.action_button.set_label(&label);
            widget_
                .action_button
                .set_action_name(notification.action_name().as_deref());
            widget_
                .action_button
                .set_action_target_value(notification.action_target().as_ref());

            widget_.action_button.show();
        }

        widget
    }

    pub(crate) fn close(&self) {
        let self_ = imp::NotificationWidget::from_instance(self);

        if let Some(source) = self_.source.borrow_mut().take() {
            glib::source_remove(source);
        };

        self.emit_by_name("close", &[]).unwrap();
    }
}
