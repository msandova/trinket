use crate::NotificationBin;

use gtk::glib;
use gtk::subclass::prelude::*;

pub trait NotificationBinImpl: WidgetImpl {}

unsafe impl<T: NotificationBinImpl> IsSubclassable<T> for NotificationBin {
    fn class_init(class: &mut glib::Class<Self>) {
        <gtk::Widget as IsSubclassable<T>>::class_init(class);
    }
    fn instance_init(instance: &mut glib::subclass::InitializingObject<T>) {
        <gtk::Widget as IsSubclassable<T>>::instance_init(instance);
    }
}
