use crate::QRCode;

use gtk::glib;
use gtk::subclass::prelude::*;

pub trait QRCodeImpl: WidgetImpl {}

unsafe impl<T: QRCodeImpl> IsSubclassable<T> for QRCode {
    fn class_init(class: &mut glib::Class<Self>) {
        <gtk::Widget as IsSubclassable<T>>::class_init(class);
    }
    fn instance_init(instance: &mut glib::subclass::InitializingObject<T>) {
        <gtk::Widget as IsSubclassable<T>>::instance_init(instance);
    }
}
