use crate::ProgressIcon;

use gtk::glib;
use gtk::subclass::prelude::*;

pub trait ProgressIconImpl: WidgetImpl {}

unsafe impl<T: ProgressIconImpl> IsSubclassable<T> for ProgressIcon {
    fn class_init(class: &mut glib::Class<Self>) {
        <gtk::Widget as IsSubclassable<T>>::class_init(class);
    }
    fn instance_init(instance: &mut glib::subclass::InitializingObject<T>) {
        <gtk::Widget as IsSubclassable<T>>::instance_init(instance);
    }
}
