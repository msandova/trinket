# Trinket
[![docs](https://docs.rs/trinket/badge.svg)](https://msandova.pages.gitlab.gnome.org/trinket/rust/trinket/) [![crates.io](https://img.shields.io/crates/v/trinket)](https://crates.io/crates/trinket)

<img src="https://gitlab.gnome.org/msandova/trinket/-/raw/master/trinket-demo/data/icons/org.gnome.TrinketDemo.svg?inline=false" width="128px" height="128px" />

GTK 4 Widget library with an exported C API and gir bindings.

## Building

To build the development version of Trinket and hack on the code
see the [general guide](https://wiki.gnome.org/Newcomers/BuildProject)
for building GNOME apps with Flatpak and GNOME Builder.

## Demo

The library comes with a demo when run with GNOME Builder.

## Documentation

Rust Docs: https://msandova.pages.gitlab.gnome.org/trinket/rust/trinket/

C Docs: https://msandova.pages.gitlab.gnome.org/trinket/
