use trinket::prelude::*;

#[test]
fn test_widgets() {
    gtk::init().unwrap();
    let progress_icon = trinket::ProgressIcon::new();
    assert_eq!(progress_icon.progress(), 0.0);
}
