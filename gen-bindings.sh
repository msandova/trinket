#!/bin/bash

cargo build --features=bindings
echo GIR
g-ir-scanner -v --warn-all --namespace Tri --nsversion=0 \
    -Iinclude --c-include "trinket.h" --library=trinket  \
    --library-path=target/debug --include=Gtk-4.0        \
    -pkg gtk-4.0 --pkg-export=libtrinket --output        \
    include/Tri-0.gir include/*.h include/*.c
echo TYPELIB
g-ir-compiler --includedir=include include/Tri-0.gir -o include/Tri-0.typelib
echo VAPIGEN
vapigen --pkg=gtk4 --library trinket include/Tri-0.gir
mv -f trinket.vapi include/trinket.vapi
cd doc; gi-docgen generate -C libtrinket.toml ../include/Tri-0.gir
