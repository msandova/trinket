#include "tri-notification-bin.h"

/**
 * TriNotificationBin:
 *
 * An overlay that displays [class@Tri.Notification]s and the content for a window.
 *
 * Notifications are displayed with tri_notification_bin_add_notification(). TriNotificationBin implementes a queue of
 * for notifications, each notification is displayed for five seconds and then the next one is displayed. TriNotificationBin is
 * intended to be a direct child of a subclass of [class@Gtk.Window] and the content for window can be set with
 * the [property@Tri.NotificationBin:child] property as follows:
 *
 * ```xml
 * <object class="GtkWindow">
 *   <child>
 *     <object class="TriNotificationBin" id="notification_bin"/>
 *       <property name="child">
 *         <!-- Rest of the widget tree. -->
 *       </property>
 *   </child>
 * </object>
 * ```
 */

/**
  * TriNotificationBin:queue-size: (attributes org.gtk.Property.get=tri_notification_bin_get_queue_size)
  *
  * The number of elements in the queue of the `TriNotificationBin`.
  */

/**
  * TriNotificationBin:child: (attributes org.gtk.Property.get=tri_notification_bin_get_child org.gtk.Property.set=tri_notification_bin_set_child)
  *
  * The child widget of the `TriNotificationBin`. This acts as the content for parent of the `TriNotificationBin`.
  */

/**
 * tri_notification_bin_icon_new:
 *
 * Creates a new `TriNotificationBin`.
 *
 * Returns: the new created `TriNotificationBin`
 */
/**
 * tri_notification_bin_add_notification:
 * @self: a `TriNotificationBin`
 *
 * Displays a @notification in @self. If @self is already displaying a notificaiton,
 * it will instead be added to the queue and be displayed later.
 */
/**
 * tri_notification_bin_get_queue_size: (attributes org.gtk.Method.get_property=queue-size)
 * @self: a `TriNotificationBin`
 *
 * Gets size of the notification queue of @self.
 *
 * Returns: the number of notifications in the queue of @self
 */
/**
 * tri_notification_bin_get_child: (attributes org.gtk.Method.get_property=child)
 * @self: a `TriNotificationBin`
 *
 * Gets the child widget of @self.
 *
 * Returns: (nullable) (transfer none): the child widget of @self
 */
/**
 * tri_notification_bin_set_child: (attributes org.gtk.Method.set_property=child)
 * @self: a `TriNotificationBin`
 * @child: (nullable): the child widget
 *
 * Sets the child widget of @self.
 */
