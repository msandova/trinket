#pragma once

#ifndef __TRI_NOTIFICATION_H__
#define __TRI_NOTIFICATION_H__

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define TRI_TYPE_NOTIFICATION            (tri_notification_get_type())
#define TRI_NOTIFICATION(obj)            (G_TYPE_CHECK_INSTANCE_CAST((obj),TRI_TYPE_NOTIFICATION,TriNotification))
#define TRI_IS_NOTIFICATION(obj)         (G_TYPE_CHECK_INSTANCE_TYPE((obj),TRI_TYPE_NOTIFICATION))
#define TRI_NOTIFICATION_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST((klass) ,TRI_TYPE_NOTIFICATION,TriNotificationClass))
#define TRI_IS_NOTIFICATION_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass) ,TRI_TYPE_NOTIFICATION))
#define TRI_NOTIFICATION_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS((obj) ,TRI_TYPE_NOTIFICATION,TriNotificationClass))

typedef struct _TriNotification      TriNotification;
typedef struct _TriNotificationClass TriNotificationClass;

struct _TriNotification {
  GtkWidget parent;
};

struct _TriNotificationClass {
  GtkWidgetClass parent_class;
};

GType tri_notification_get_type (void);

GtkWidget *tri_notification_new (const char *title) G_GNUC_WARN_UNUSED_RESULT;

void tri_notification_set_button (TriNotification *self, const char *label, const char *detailed_action);
void tri_notification_set_button_with_target_value (TriNotification *self, const char *label, const char *action, GVariant *target);

G_END_DECLS

#endif // __TRI-NOTIFICATION_H__
