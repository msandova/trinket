#pragma once

#ifndef __TRI_NOTIFICATION_BIN_H__
#define __TRI_NOTIFICATION_BIN_H__

#include <gtk/gtk.h>
#include "tri-notification.h"

G_BEGIN_DECLS

#define TRI_TYPE_NOTIFICATION_BIN            (tri_notification_bin_get_type())
#define TRI_NOTIFICATION_BIN(obj)            (G_TYPE_CHECK_INSTANCE_CAST((obj),TRI_TYPE_NOTIFICATION_BIN,TriNotificationBin))
#define TRI_IS_NOTIFICATION_BIN(obj)         (G_TYPE_CHECK_INSTANCE_TYPE((obj),TRI_TYPE_NOTIFICATION_BIN))
#define TRI_NOTIFICATION_BIN_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST((klass) ,TRI_TYPE_NOTIFICATION_BIN,TriNotificationBinClass))
#define TRI_IS_NOTIFICATION_BIN_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass) ,TRI_TYPE_NOTIFICATION_BIN))
#define TRI_NOTIFICATION_BIN_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS((obj) ,TRI_TYPE_NOTIFICATION_BIN,TriNotificationBinClass))

typedef struct _TriNotificationBin      TriNotificationBin;
typedef struct _TriNotificationBinClass TriNotificationBinClass;

struct _TriNotificationBin {
  GtkWidget parent;
};

struct _TriNotificationBinClass {
  GtkWidgetClass parent_class;
};

GType tri_notification_bin_get_type (void);

GtkWidget *tri_notification_bin_new (void) G_GNUC_WARN_UNUSED_RESULT;

void tri_notification_bin_add_notification (TriNotificationBin *self, TriNotification *notification);

guint tri_notification_bin_get_queue_size (TriNotificationBin *self);

GtkWidget *tri_notification_bin_get_child (TriNotificationBin *self);
void       tri_notification_bin_set_child (TriNotificationBin *self, GtkWidget *child);

G_END_DECLS

#endif // __TRI_NOTIFICATION_BIN_H__
