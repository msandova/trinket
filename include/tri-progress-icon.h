#pragma once

#ifndef __TRI_PROGRESS_ICON_H__
#define __TRI_PROGRESS_ICON_H__

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define TRI_TYPE_PROGRESS_ICON            (tri_progress_icon_get_type())
#define TRI_PROGRESS_ICON(obj)            (G_TYPE_CHECK_INSTANCE_CAST((obj),TRI_TYPE_PROGRESS_ICON,TriProgressIcon))
#define TRI_IS_PROGRESS_ICON(obj)         (G_TYPE_CHECK_INSTANCE_TYPE((obj),TRI_TYPE_PROGRESS_ICON))
#define TRI_PROGRESS_ICON_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST((klass) ,TRI_TYPE_PROGRESS_ICON,TriProgressIconClass))
#define TRI_IS_PROGRESS_ICON_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass) ,TRI_TYPE_PROGRESS_ICON))
#define TRI_PROGRESS_ICON_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS((obj) ,TRI_TYPE_PROGRESS_ICON,TriProgressIconClass))

typedef struct _TriProgressIcon      TriProgressIcon;
typedef struct _TriProgressIconClass TriProgressIconClass;

struct _TriProgressIcon {
  GtkWidget parent;
};

struct _TriProgressIconClass {
  GtkWidgetClass parent_class;
};

GType tri_progress_icon_get_type (void);

GtkWidget *tri_progress_icon_new (void) G_GNUC_WARN_UNUSED_RESULT;

float tri_progress_icon_get_progress (TriProgressIcon *self);
void  tri_progress_icon_set_progress (TriProgressIcon *self, float progress);

bool tri_progress_icon_get_inverted (TriProgressIcon *self);
void tri_progress_icon_set_inverted (TriProgressIcon *self, bool inverted);

bool tri_progress_icon_get_clockwise (TriProgressIcon *self);
void tri_progress_icon_set_clockwise (TriProgressIcon *self, bool clockwise);

G_END_DECLS

#endif /* __TRI_PROGRESS_ICON_H__ */
