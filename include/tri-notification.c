#include "tri-notification.h"

/**
 * TriNotification:
 *
 * A notification object to be consumed by [class@Tri.NotificationBin].
 *
 * To display a notification with an action button either tri_notification_set_button() or
 * tri_notification_set_button_with_target_value() can be used.
 */
/**
 * tri_notification_new:
 * @title: the title to be displayed
 *
 * Creates a new `TriNotification` which will display @title as its text.
 *
 * Returns: the new created `TriProgressIcon`
 */
/**
 * tri_notification_set_button:
 * @self: a `TriNotification`
 * @label: The button label
 * @detailed_action: The detailed action name
 *
 * Sets a button to be displayed of @self that activates the action in @detailed_action when clicked.
 * See g_action_parse_detailed_name() for a description of the format of @detailed_action.
 */
/**
 * tri_notification_set_button_with_target_value:
 * @self: a `TriNotification`
 * @label: The button label
 * @action: The action name
 * @target: (nullable): target to use for @action's parameter, or @NULL
 *
 * Sets a button to be displayed of @self that activates @action when clicked.
 * If @target is non-@NULL, `action` will be activated with @target as its parameter.
 */
