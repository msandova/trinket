#include <gtk/gtk.h>

#ifndef __TRINKET_H__
#define __TRI_H__

G_BEGIN_DECLS

#include "tri-progress-icon.h"
#include "tri-notification.h"
#include "tri-notification-bin.h"
#include "tri-qr-code.h"

G_END_DECLS

void tri_init (void);

#endif /* __TRI_H__ */
