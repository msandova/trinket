#include "tri-qr-code.h"


/**
 * TriQRCode:
 *
 * A widget that display a QR Code.
 *
 * The QR code of `TriQRCode` is set with the tri_qr_code_set_bytes()
 * method. It is recommended for a QR Code to have a quiet zone, i.e. a margin of
 * four times the value of [property@Tri.QRCode:block-size], in most contexts, widgets
 * already count with such a margin.
 *
 * The code can be themed via css, where the quiet-zone
 * can be set as a padding:
 *
 * ```css
 * .qrcode {
 *     color: black;
 *     background: white;
 *     padding: 24px;
 * }
 * ```
 */
/**
  * TriQRCode:block-size: (attributes org.gtk.Property.get=tri_qr_code_get_block_size org.gtk.Property.set=tri_qr_code_set_block_size)
  *
  * The block size of the `TriQRCode`. This determines the size of the the widget.
  */
/**
 * tri_qr_code_new:
 *
 * Creates a new `TriQRCode`.
 *
 * Returns: the new created `TriQRCode`
 */

/**
 * tri_qr_code_new_from_bytes:
 *
 * Creates a new `TriQRCode` with a QR code generated from @bytes.
 *
 * Returns: the new created `TriQRCode`
 */
/**
 * tri_qr_code_set_bytes:
 * @self: a `TriQRCode`
 * @bytes: Data to generate the QR code
 *
 * Sets the displayed code of @self to a QR code generated from @bytes.
 */
/**
 * tri_qr_code_get_block_size: (attributes org.gtk.Method.get_property=block-size)
 * @self: a `TriQRCode`
 *
 * Gets the block size @self.
 *
 * Returns: the block size of @self
 */
/**
 * tri_qr_code_set_block_size: (attributes org.gtk.Method.set_property=block-size)
 * @self: a `TriQRCode`
 * @block_size: block size for @self
 *
 * Sets the block size of @self. @block_size should be bigger than `1`.
 */
/**
 * tri_qr_code_save_to_png: (attributes org.gtk.Method.set_property=block-size)
 * @self: a `TriQRCode`
 * @location: where to store the image
 *
 * Saves the displayed QR code to @location.
 *
 * Returns: %TRUE if the operation finished successfully.
 */
