#include "trinket.h"

/**
 * tri_init:
 *
 * Initializes Libtrinket.
 *
 * Call this function just after initializing GTK, if you are using
 * [class@Gtk.Application] it means it must be called when the
 * [signal@Gio.Application::startup] signal is emitted. There's no need to call this
 *
 * If Libtrinket has already been initialized, the function will simply return.
 *
 * This makes sure translations, types, themes, and icons for the Trinket
 * library are set up properly.
 */
