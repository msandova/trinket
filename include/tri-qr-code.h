#pragma once

#ifndef __TRI_QR_CODE_H__
#define __TRI_QR_CODE_H__

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define TRI_TYPE_QR_CODE            (tri_qr_code_get_type())
#define TRI_QR_CODE(obj)            (G_TYPE_CHECK_INSTANCE_CAST((obj),TRI_TYPE_QR_CODE,TriQrcode))
#define TRI_IS_QR_CODE(obj)         (G_TYPE_CHECK_INSTANCE_TYPE((obj),TRI_TYPE_QR_CODE))
#define TRI_QR_CODE_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST((klass) ,TRI_TYPE_QR_CODE,TriQrcodeClass))
#define TRI_IS_QR_CODE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass) ,TRI_TYPE_QR_CODE))
#define TRI_QR_CODE_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS((obj) ,TRI_TYPE_QR_CODE,TriQrcodeClass))

typedef struct _TriQRCode      TriQRCode;
typedef struct _TriQRCodeClass TriQRCodeClass;

struct _TriQRCode {
  GtkWidget parent;
};

struct _TriQRCodeClass {
  GtkWidgetClass parent_class;
};

GType tri_qr_code_get_type (void);

GtkWidget *tri_qr_code_new (void) G_GNUC_WARN_UNUSED_RESULT;
GtkWidget *tri_qr_code_new_from_bytes (GBytes *bytes) G_GNUC_WARN_UNUSED_RESULT;

void  tri_qr_code_set_bytes (TriQRCode *self, GBytes *bytes);

guint tri_qr_code_get_block_size (TriQRCode *self);
void tri_qr_code_set_block_size (TriQRCode *self, guint block_size);

bool tri_qr_code_save_to_png (TriQRCode *self, const char *location);

G_END_DECLS

#endif // __TRI_QR_CODE_H__
