#include "tri-progress-icon.h"

/**
 * TriProgressIcon:
 *
 * A widget to display the progress of an operation.
 *
 * The [property@Tri.ProgressIcon:progress] property of `TriProgressIcon`
 * is a floating number between 0.0 and 1.0, inclusive which denotes that the
 * operation has started or finished, respectively.
 */

/**
  * TriProgressIcon:progress: (attributes org.gtk.Property.get=tri_progress_icon_get_progress org.gtk.Property.set=tri_progress_icon_set_progress)
  *
  * The progress of the `TriProgressIcon`.
  */

/**
  * TriProgressIcon:inverted: (attributes org.gtk.Property.get=tri_progress_icon_get_inverted org.gtk.Property.set=tri_progress_icon_set_inverted)
  *
  * Whether `TriProgressIcon` progress is inverted.
  */

/**
  * TriProgressIcon:clockwise: (attributes org.gtk.Property.get=tri_progress_icon_get_clockwise org.gtk.Property.set=tri_progress_icon_set_clockwise)
  *
  * Whether the `TriProgressIcon` progress is filled in the clockwise direction.
  */

/**
 * tri_progress_icon_new:
 *
 * Creates a new `TriProgressIcon`.
 *
 * Returns: the new created `TriProgressIcon`
 */

/**
 * tri_progress_icon_get_progress: (attributes org.gtk.Method.get_property=progress)
 * @self: a `TriProgressIcon`
 *
 * Gets the child widget of @self.
 *
 * Returns: the progress of @self
 */

/**
 * tri_progress_icon_set_progress: (attributes org.gtk.Method.set_property=progress)
 * @self: a `TriProgressIcon`
 * @progress: Fraction of the task that has been completed
 *
 * Sets the progress of @self. @progress should be between 0.0 and 1.0, inclusive.
 */

/**
 * tri_progress_icon_get_inverted: (attributes org.gtk.Method.get_property=inverted)
 * @self: a `TriProgressIcon`
 *
 * Returns whether @self is inverted.
 *
 * Returns: %TRUE if @self is inverted
 */

/**
 * tri_progress_icon_set_inverted: (attributes org.gtk.Method.set_property=inverted)
 * @self: a `TriProgressIcon`
 * @inverted: %TRUE to invert @self
 *
 * Sets whether @self is inverted.
 */

/**
 * tri_progress_icon_get_clockwise: (attributes org.gtk.Method.get_property=clockwise)
 * @self: a `TriProgressIcon`
 *
 * Returns the completion direction of @self.
 *
 * Returns: %TRUE if @self's progress is displayed clockwise
 */

/**
 * tri_progress_icon_set_clockwise: (attributes org.gtk.Method.set_property=clockwise)
 * @self: a `TriProgressIcon`
 * @clockwise: %TRUE if @self progress is displayed clockwise
 *
 * Sets the progress display direction of @self.
 */
