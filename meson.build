project(
  'trinket-demo',
  ['rust', 'c'],
  version: '0.0.1',
  meson_version: '>= 0.56',
  license: 'LGPL-3.0-or-later',
)

version_arr = meson.project_version().split('-')[0].split('.')
api_version = version_arr[0]

project_name = 'trinket' # TODO Use meson.project_name()
package_api_name = 'libtrinket'
package_namespace = 'Tri'

build_root = meson.current_build_dir()
source_root = meson.current_source_dir()

i18n = import('i18n')
gnome = import('gnome')

base_id = 'org.gnome.TrinketDemo'

dependency('glib-2.0', version: '>= 2.64')
dependency('gio-2.0', version: '>= 2.64')
dependency('gtk4', version: '>= 4.5.0')

glib_compile_resources = find_program('glib-compile-resources', required: true)
glib_compile_schemas = find_program('glib-compile-schemas', required: true)
desktop_file_validate = find_program('desktop-file-validate', required: false)
appstream_util = find_program('appstream-util', required: false)
cargo = find_program('cargo', required: true)

version = meson.project_version()
version_array = version.split('.')
major_version = version_array[0].to_int()
minor_version = version_array[1].to_int()
version_micro = version_array[2].to_int()

prefix = get_option('prefix')
bindir = prefix / get_option('bindir')
libdir = prefix / get_option('libdir')
localedir = prefix / get_option('localedir')

girdir     = prefix / get_option('datadir') / 'gir-1.0'
typelibdir = prefix / get_option('libdir')  / 'girepository-1.0'
vapidir    = prefix / get_option('datadir') / 'vala' / 'vapi'

datadir = prefix / get_option('datadir')
pkgdatadir = datadir / meson.project_name()
iconsdir = datadir / 'icons'
gettext_package = meson.project_name()

if get_option('profile') == 'development'
  profile = 'Devel'
  vcs_tag = run_command('git', 'rev-parse', '--short', 'HEAD').stdout().strip()
  if vcs_tag == ''
    version_suffix = '-devel'
  else
    version_suffix = '-@0@'.format(vcs_tag)
  endif
  application_id = '@0@.@1@'.format(base_id, profile)
else
  profile = ''
  version_suffix = ''
  application_id = base_id
endif

generate_bindings = get_option('bindings')
cargo_script = find_program('build-aux/cargo.sh')

subdir('include')
if get_option('rust-lib')
  subdir('src')
endif

if get_option('examples')
  subdir('trinket-demo')
endif

if get_option('gtk_doc')
  subdir('doc')
endif
